package sqlutil

import "database/sql"

// Execer is an interface for the sql Exec method.
type Execer interface {
	Exec(query string, args ...interface{}) (sql.Result, error)
}

// Queryer is an interface for the sql Query method.
type Queryer interface {
	Query(query string, args ...interface{}) (*sql.Rows, error)
}

// QueryRower is an interface for the sql QueryRow method.
type QueryRower interface {
	QueryRow(query string, args ...interface{}) *sql.Row
}
