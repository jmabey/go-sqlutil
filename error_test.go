package sqlutil

import (
	"errors"
	"testing"

	"github.com/lib/pq"
)

func TestCheckViolation(t *testing.T) {
	for i, tt := range []struct {
		in  error
		out bool
	}{
		{&pq.Error{Code: CodeCheckViolation, Constraint: "constraint"}, true},
		{&pq.Error{Code: CodeCheckViolation}, false},
		{&pq.Error{Constraint: "constraint"}, false},
		{errors.New("error"), false},
		{nil, false},
	} {
		if got := CheckViolation(tt.in, "constraint"); got != tt.out {
			t.Errorf("%d: got %t; want %t", i, got, tt.out)
		}
	}
}

func TestUniqueViolation(t *testing.T) {
	for i, tt := range []struct {
		in  error
		out bool
	}{
		{&pq.Error{Code: CodeUniqueViolation, Constraint: "constraint"}, true},
		{&pq.Error{Code: CodeUniqueViolation}, false},
		{&pq.Error{Constraint: "constraint"}, false},
		{errors.New("error"), false},
		{nil, false},
	} {
		if got := UniqueViolation(tt.in, "constraint"); got != tt.out {
			t.Errorf("%d: got %t; want %t", i, got, tt.out)
		}
	}
}
