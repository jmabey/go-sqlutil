package sqlutil

import "github.com/lib/pq"

// PostgreSQL error codes.
const (
	CodeUniqueViolation = "23505"
	CodeCheckViolation  = "23514"
)

// CheckViolation returns true if the given error is a check violation on
// constraint c.
func CheckViolation(err error, c string) bool {
	if err, ok := err.(*pq.Error); ok {
		return err.Code == CodeCheckViolation && err.Constraint == c
	}
	return false
}

// UniqueViolation returns true if the given error is a unique violation on
// constraint c.
func UniqueViolation(err error, c string) bool {
	if err, ok := err.(*pq.Error); ok {
		return err.Code == CodeUniqueViolation && err.Constraint == c
	}
	return false
}
