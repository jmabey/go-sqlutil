package sqlutil

import (
	"database/sql"
	"testing"

	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

const (
	RegexpSelect    = "SELECT"
	QuerySelectOne  = "SELECT id FROM models WHERE id = ?;"
	QuerySelectMany = "SELECT id FROM models;"
)

type Model struct {
	ID int
}

func NewModelRows() sqlmock.Rows {
	return sqlmock.NewRows([]string{"id"})
}

func ScanModel(s Scanner) (interface{}, error) {
	m := &Model{}
	return m, s.Scan(&m.ID)
}

func TestQueryOne(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	rs := NewModelRows().AddRow(1)
	mock.ExpectQuery(RegexpSelect).WillReturnRows(rs)

	iface, err := QueryOne(db, ScanModel, QuerySelectOne, 1)
	if err != nil {
		t.Errorf("unexpected error %q", err)
	}
	if m := iface.(*Model); m.ID != 1 {
		t.Errorf("model not scanned")
	}
}

func TestQueryOneNoRows(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	rs := NewModelRows()
	mock.ExpectQuery(RegexpSelect).WillReturnRows(rs)

	_, err = QueryOne(db, ScanModel, QuerySelectOne, 1)
	if err == nil || err != sql.ErrNoRows {
		t.Errorf("got error %q; want %q", err, sql.ErrNoRows)
	}
}

func TestQueryMany(t *testing.T) {
	db, mock, err := sqlmock.New()
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	rs := NewModelRows().AddRow(1).AddRow(2)
	mock.ExpectQuery(RegexpSelect).WillReturnRows(rs)

	is, err := QueryMany(db, ScanModel, QuerySelectMany)
	if err != nil {
		t.Errorf("unexpected error %q", err)
	}
	if got, want := len(is), 2; got != want {
		t.Fatalf("got %d length; want %d", got, want)
	}
	for i := 0; i < 2; i++ {
		if m, want := is[i].(*Model), i+1; m.ID != want {
			t.Errorf("got model[%d].ID %d; want %d", i, m.ID, want)
		}
	}
}
