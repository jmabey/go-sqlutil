package sqlutil

import (
	"database/sql"
	"log"
)

// Scanner is an interface for the sql Row.Scan and Rows.Scan methods.
type Scanner interface {
	Scan(dest ...interface{}) error
}

// ScanFunc is a function that scans a row into a struct and returns the struct
// as an interface{}.
type ScanFunc func(s Scanner) (interface{}, error)

// QueryOne executes a query and scans it into a single struct.
func QueryOne(q QueryRower, sf ScanFunc, qs string, args ...interface{}) (interface{}, error) {
	r := q.QueryRow(qs, args...)
	s, err := sf(r)
	if err != nil {
		if err != sql.ErrNoRows {
			log.Printf("sqlutil.QueryOne with query %s: %s", qs, err)
		}
		return nil, err
	}
	return s, nil
}

// QueryMany executes a query and scans all rows into a slice of structs.
func QueryMany(q Queryer, sf ScanFunc, qs string, args ...interface{}) ([]interface{}, error) {
	rs, err := q.Query(qs, args...)
	if err != nil {
		log.Printf("sqlutil.QueryMany with query %s: %s", qs, err)
		return nil, err
	}
	defer rs.Close()

	ss := []interface{}{}
	for rs.Next() {
		s, err := sf(rs)
		if err != nil {
			return nil, err
		}
		ss = append(ss, s)
	}
	if err := rs.Err(); err != nil {
		return nil, err
	}

	return ss, nil
}
